#include "player.h"
#include <vector>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
 
#include <iostream>
#include <vector>

#define CORNER 10000000;
#define ONE_FROM_SIDE 3;
#define CORNER_ADJ 0;
#define CORNER_DIAG 0;
#define CORNER_2_SIDE 24;
#define CORNER_2_DIAG 21;
#define CORNER_3_SIDE 15;
#define NOT_CORNER 12;

Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp
    for(int r = 2; r <= 5; r++)
        for(int c = 2; c <= 5; c++)
            if(!((r == 3 || r == 4) && (c == 3 || c == 4)))
            {
                possMoveVect.push_back(new Move(r, c));
            }
    
    testingMinimax = false;
    myColor = side;
    oppColor = BLACK;
    if(side == BLACK)
		oppColor = WHITE;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete &possMoveVect;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
    std::cerr << "START" << std::endl;
    int score;
    int x, y;
    int myMoveScore = -10000000;
    if(opponentsMove != NULL)
    {
		board.doMove(opponentsMove, oppColor);
        updatePossMoves(opponentsMove);
	}
	if(!board.hasMoves(myColor))
		return NULL;
	for(int j = 0; j < 8; j++)
	{
		for(int k = 0; k < 8; k++)
		{
			Move *temp = new Move(j, k);
			if(board.checkMove(temp, myColor))
			{
				score = moveScore(temp, myColor);
				if(score > myMoveScore)
				{
					x = j;
					y = k;
					myMoveScore = score;
				}
			}
			delete temp;
		}
	}
	Move *myMove = new Move(x, y);
	board.doMove(myMove, myColor);
    updatePossMoves(myMove);
    if(myMove == NULL)
		std::cerr << "here" << std::endl;
    return myMove;
}

int Player::newTiles(Move *move, Side side, Board board)
{
	int flipped = 0;
	int temp;
	int x, y;
	for(int i = -1; i < 2; i++)
	{
		for(int j = -1; j < 2; j++)
		{
			x = move->x;
			y = move->y;
			if(i == 0 && j == 0)
				continue;
			temp = 0;
			while(true)
			{
				x += i;
				y += j;
				// Reached end, nothing gets flipped
				if(x < 0 || x > 7 || y < 0 || y > 7)
				{
					temp = 0;
					break;
				}
				if(board.occupied(x, y) && board.get(side, x, y))
				{
					// We turned over a CORNER_ADJ (bad)
					if(((x == 1 || x == 6) && (y == 0 || y == 7)) || ((x == 0 || x == 7) && (y == 1 || y == 6)))
						temp = -2;
					// We turned over a CORNER_DIAG (also bad)
					else if((x == 1 || x == 6) && (y == 1 || y == 6))
						temp = -1;
					else
						temp++;
				}
				else
					break;
			}
			flipped += temp;
		}
	}
	flipped++;
	return flipped;
}

int Player::minimax(Move *move, Side side, Board board, int iters)
{
	if(iters == 0)
		return 0;
	Side otherSide;
	otherSide = BLACK;
	if(side == BLACK)
		otherSide = WHITE;
	Board *tempBoard = board.copy();
	tempBoard->doMove(move, side);
	int score = 0;
	int scalar = -1;
	if(side == myColor)
		scalar = 1;
	for(int j = 0; j < 8; j++)
	{
		for(int k = 0; k < 8; k++)
		{
			Move *temporMove = new Move(j, k);
			score += newTiles(temporMove, side, *tempBoard) + scalar * minimax(temporMove, otherSide, *tempBoard, iters--);
			delete temporMove;
		}
	}
	return score;
}

int Player::moveScore(Move *move, Side side)
{
	if(!board.checkMove(move, side))
		return -10000;
	int x = move->x;
	int y = move->y;
	int its = 5;
	// Check if it's a corner
	if((x == 0 || x == 7) && (y == 0 || y == 7))
	{
		return CORNER + minimax(move, side, board, its);
	}
	// Check if it's adjacent to a corner
	if(((x == 1 || x == 6) && (y == 0 || y == 7)) || ((x == 0 || x == 7) && (y == 1 || y == 6)))
	{
		return CORNER_ADJ + minimax(move, side, board, its);
	}
	// Check if it's diagonal from a corner
	if((x == 1 || x == 6) && (y == 1 || y == 6))
	{
		return CORNER_DIAG + minimax(move, side, board, its);
	}
	// Check if it's two away from corner and on edge
	if(((x == 2 || x == 5) && (y == 0 || y == 7)) || ((x == 0 || x == 7) && (y == 2 || y == 5)))
	{
		return CORNER_2_SIDE + minimax(move, side, board, its);
	}
	// Check if it's two away from corner diagonally
	if((x == 2 || x == 5) && (y == 2 || y == 5))
	{
		return CORNER_2_DIAG + minimax(move, side, board, its);
	}
	// Check if it's on side 3 away from corner
	if(((x == 3 || x == 4) && (y == 0 || y == 7)) || ((x == 0 || x == 7) && (y == 3 || y == 4)))
	{
		return CORNER_3_SIDE + minimax(move, side, board, its);
	}
	// Check if it's one from side
	if(x == 1 || x == 6 || y == 1 || y == 6)
	{
		return ONE_FROM_SIDE + minimax(move, side, board, its);
	}
	// Not a special spot
	return NOT_CORNER + minimax(move, side, board, its);
}

/**
 * @brief this function updates the list of possible moves either player can
 * make after a move is made by either player
 */
void Player::updatePossMoves(Move *move)
{
    int x = move->getX();
    int y = move->getY();

    Move *temp;
    for(i = possMoveVect.begin(); i != possMoveVect.end(); i++)
    {
        temp = *i;
        if(temp->getX() == x && temp->getY() == y)
        {
            possMoveVect.erase(i);
            board.removePossMove(x, y);
            break;
        }
    }
    for(int r = -1; r <= 1; r++)
        for(int c = -1; c <= 1; c++)
            if((r != 0 && c != 0) && board.onBoard(x + r, y + c))
            {
                if(!board.checkPossMove(x + r, y + c)
                && !board.occupied(x + r, y + c))
                {
                    board.addPossMove(x + r, y + c);
                    possMoveVect.push_back(new Move(x + r, y + c));
                }
            }
}
