***README***
* project authors *
* Parker Won (jwon@caltech.edu), Tim Menninger(tmenning@caltech.edu)

* Contributions *
* Parker:
* - Came up with idea for the heuristic we used(discussed below)
* - Implemented possMove into the code (discussed below)
*
* Tim:
* - Coded most of the heuristic implementation
* - Implemented minimax

* Strategy *
* One of the main stradegies we incorporated into our heuristic revolved
* around the four corners of the board. We already knew that taking the four
* corners is extremely important in winning the game but we also realized that
* NOT taking any square adjacent (including diagonal direction) to the four
* corners. This is because not taking these squares will force the opponent
* to take it and open up an opportunity to take the corner square.
*
* Another strategy we used was avoiding taking squares that are right adjacent
* to a square on any of the four sides. This forces the apponent to take said
* square which allows us to take the square on the edge. Edge squares are
* safer than other squares.
*
* We also tried to be fancy and keep track of all possible moves so that we
* didn't have to check all 64 squares every turn. This idea was scrapped
* since it complicated the rest of the code too much.
