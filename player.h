#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Side myColor;
    Side oppColor;
    std::vector <Move*> possMoveVect;
    std::vector <Move*>::iterator i;
    Board board;

    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int minimax(Move *move, Side side, Board board, int iters);

    int newTiles(Move *move, Side side, Board board);
    int moveScore(Move *move, Side side);

    void updatePossMoves(Move *move);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
