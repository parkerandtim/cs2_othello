#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;
    bitset<64> possMove;
       
public:
    Board();
    ~Board();
    Board *copy();

    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);

    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();

    void addPossMove(int x, int y);
    void removePossMove(int x, int y);
    bool checkPossMove(int x, int y);

    void setBoard(char data[]);
};

#endif
